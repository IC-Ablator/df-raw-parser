'use strict'

const rawLexer = require('./lexer')
const CstParser = require('chevrotain').CstParser
const tokenVocabulary = rawLexer.tokenVocabulary

const Filename = tokenVocabulary.Filename
const Lbracket = tokenVocabulary.Lbracket
const Rbracket = tokenVocabulary.Rbracket
const Separator = tokenVocabulary.Separator
const Argument = tokenVocabulary.Argument
const Newline = tokenVocabulary.Newline

class RawParser extends CstParser {
  constructor () {
    super(tokenVocabulary)

    // for conciseness
    const $ = this

    $.RULE('rawFile', () => {
      $.CONSUME(Filename)
      $.OPTION(() => {
        $.SUBRULE($.comment)
      })
      $.SUBRULE($.contents)
    })

    $.RULE('contents', () => {
      $.MANY(() => {
        $.SUBRULE($.tagDef)
        $.OPTION(() => {
          $.SUBRULE($.comment)
        })
      })
    })
    // the lexer tokenizes the comments but the parser discards them as it depends more on where they are located than their structure
    $.RULE('comment', () => {
      $.MANY(() => {
        $.OR([
          { ALT: () => $.CONSUME(Newline) },
          { ALT: () => $.CONSUME(Argument) }
        ])
      })
    })

    $.RULE('tagDef', () => {
      $.CONSUME(Lbracket)
      $.CONSUME(Argument) // this is the tag name
      $.MANY(() => {
        $.SUBRULE($.argDef)
      })
      $.CONSUME(Rbracket)
    })

    $.RULE('argDef', () => {
      $.CONSUME(Separator)
      $.CONSUME(Argument) // these are the arguments
    })

    // very important to call this after all the rules have been defined.
    // otherwise the parser may not work correctly as it will lack information
    // derived during the self anaylsis phase
    this.performSelfAnalysis()
  }
}

// reuse the same parser instance.
const parserInstance = new RawParser()

module.exports = {
  parserInstance: parserInstance,

  RawParser: RawParser,

  parse: function (inputText) {
    const lexResult = rawLexer.lex(inputText)

    // ".input" is a setter which will reset the parser's internal's state
    parserInstance.input = lexResult.tokens

    // No semantic actions so this won't return anything yet.
    parserInstance.rawFile()

    if (parserInstance.errors.length > 0) {
      throw Error('Parsing errors detected: ' + parserInstance.errors[0].message)
    }
  }
}
