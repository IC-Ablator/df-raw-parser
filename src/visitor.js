'use strict'

const rawLexer = require('./lexer')
// re-using the parser implemented in step two.
const parser = require('./parser')
const RawParser = parser.RawParser

// A new parser instance with CST output (enabled by default)
const parserInstance = new RawParser([])
// The base visitor class can be accessed via the a parser instance
const BaseRawVisitor = parserInstance.getBaseCstVisitorConstructor()

class RawToAstVisitor extends BaseRawVisitor {
  constructor () {
    super()
    this.validateVisitor()
  }

  // rawfile = Filename + tagdefs
  // tagdefs = tagname + arguments

  // visitor methods
  rawFile (ctx) {
    const Filename = ctx.Filename[0]
    const tags = this.visit(ctx.contents)
    return {
      type: 'FILE_NAME',
      Filename: Filename.image,
      tags: tags.tagDef
    }
  }

  contents (ctx) {
    const tags = []
    ctx.tagDef.forEach(element => {
      tags.push(this.visit(element))
    })
    return {
      type: 'TAG_DEFS',
      tagDef: tags
    }
  }

  tagDef (ctx) {
    const tag = ctx.Argument[0]
    if (ctx.argDef === undefined) {
      const args = this.visit(ctx.argDef)
      return {
        type: 'TAG',
        name: tag.image,
        args: args,
        startColumn: tag.startColumn,
        endColumn: tag.endColumn,
        startLine: tag.startLine,
        endLine: tag.endLine
      }
    } else {
      const args = []
      ctx.argDef.forEach(element => {
        args.push(this.visit(element))
      })
      return {
        type: 'TAG',
        name: tag.image,
        args: args,
        startColumn: tag.startColumn,
        endColumn: tag.endColumn,
        startLine: tag.startLine,
        endLine: tag.endLine
      }
    }
  }

  argDef (ctx) {
    const argument = ctx.Argument[0]
    return {
      type: 'ARGUMENT',
      value: argument.image,
      startColumn: argument.startColumn,
      endColumn: argument.endColumn,
      startLine: argument.startLine,
      endLine: argument.endLine
    }
  }

  // never visited
  comment (ctx) {
    const comment = this.visit(ctx.Argument)
    return {
      type: 'COMMENT',
      comment: comment
    }
  }
}

// Our visitor has no state, so a single instance is sufficient
const toAstVisitorInstance = new RawToAstVisitor()

module.exports = {
  toAst: function (inputText) {
    const lexResult = rawLexer.lex(inputText)

    // ".input" is a setter which will reset the parser's internal's state
    parserInstance.input = lexResult.tokens

    // Automatic CST created when parsing
    const cst = parserInstance.rawFile()

    if (parserInstance.errors.length > 0) {
      throw Error('Parsing errors detected: ' + parserInstance.errors[0].message)
    }

    const ast = toAstVisitorInstance.visit(cst)

    return ast
  }
}
