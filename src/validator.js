const fs = require('fs')
const rawAST = require('./main').rawAST

fs.readFile('./output/scraper_out.json', (err, data) => {
  if (err) throw err

  const specDwarf = JSON.parse(data)
  // loop through rawAST tags
  for (let i = 0; i < rawAST.tags.length; i++) {
    const astTag = rawAST.tags[i].name // tag name
    const specTag = specDwarf[astTag]

    if (specTag !== undefined) { // check if tag exists
      // check arguments
      validateArgs(specTag.args, astTag.args)
    }
  }
  console.log('.')
})

function validateArgs (specArgs, astArgs) {
  // args[0]
  // specArgs[i].data
  // data can be "string", "integer", [string,integer,constant]
  // specArgs needs more properties like what's the minimum and maximum values allowed for integers
  // astArgs[i].value

  // check if the TAG actually needs args
  if (specArgs.length > 0) {
    if (astArgs.args) {
      const i = 1 // need to traverse the argument list

      // we need to check if the data is an array of constants or just one data type
      if (Array.isArray(specArgs[i].data)) {
        // need to traverse the allowed data
        for (let n = 0; n < specArgs[i].data.length; n++) {
          validateType(specArgs[i].data[n], astArgs[i].value)
        }
      } else {
        validateType(specArgs[i].data, astArgs[i].value)
      }
      // evaluate arguments function to see if the arguments are optional or required
      // check ast against the spec data type
      switch (specArgs[i].data) {
        case 'integer':
          if (!astArgs[i].value.match(/^\d+$/)) {
            throw new Error('Expected integer')
          }
          break
        case 'string':
          if (!astArgs[i].value) {
            throw new Error('Expected string')
          }
          break
        default:
          if (astArgs[i].value != specArgs[i].data) {
            throw new Error('Expected constant')
          }
          break
      }
      // the function should count how many arguments were used
    } else {
      // maybe check if the arguments are all optional
      // CONFIRM IF OPTIONAL ARGS ARE EMPTY STRINGS ON OFFICIAL RAWS (yes they are)
      // otherwise throw missing arguments error
      throw new Error('Arguments missing')
    }
  } else if (astArgs.args) { // no spec args but ast args exist
    // throw unexpected argument error
    throw new Error('Unexpected argument')
  }
  // needs another function that handles the error reporting
}

function validateType (spec, value) {
  // spec is the data or array of data 'sring ['string', 'STP']
  if (Array.isArray(spec)) {
    spec.forEach(function (element) {
      if (value === 'string') { if (value === element) return value }
    })
  }
}
