const toAstVisitor = require('./visitor').toAst
const inputText = `item_ammo
a note
[OBJECT:ITEM_AMMO] thisisacomment

    [ITEM_AMMO:AMMO_ARROW]
    [WOOD]
    [SPEED:123]
    [SIZE:60:100:200]
    lulcomment`

const rawAST = toAstVisitor(inputText)
console.log(JSON.stringify(rawAST, null, '\t'))

exports.rawAST = rawAST
